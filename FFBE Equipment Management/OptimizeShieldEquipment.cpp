#include "OptimizeShieldEquipment.h"

namespace FFBE
{

    #pragma region Constructors & Destructor

    OptimizeShieldEquipment::OptimizeShieldEquipment(
        Stats::StatType first ,
        Stats::StatType second ,
        Stats::StatType third ,
        Stats::StatType fourth ,
        Stats::StatType fifth ,
        Stats::StatType sixth )
    {
        _statsPriority.first = first;
        _statsPriority.second = second;
        _statsPriority.third = third;
        _statsPriority.fourth = fourth;
        _statsPriority.fifth = fifth;
        _statsPriority.sixth = sixth;
    }

    #pragma endregion

    #pragma region Override Methods

    bool OptimizeShieldEquipment::canEquip( 
        const std::shared_ptr<Equipment> equipment , 
        const EquipableFlags & equipableEquipmentFlag ) const
    {
        auto equipmentFlag = std::dynamic_pointer_cast<HandEquipment>( equipment )->getShieldType();
        auto matchEquipmentType = uint32_t( equipableEquipmentFlag.Shield & equipmentFlag );

        return ( matchEquipmentType > 0 );
    }

    #pragma endregion

}