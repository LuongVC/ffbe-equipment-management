#include "StatsPriority.h"

namespace FFBE
{
    const double StatsPriority::firstMultiplier = 1.00;
    const double StatsPriority::secondMultiplier = 0.25;
    const double StatsPriority::thirdMultiplier = 0.10;
    const double StatsPriority::fourthMultiplier = 0.00;
    const double StatsPriority::fifthMultiplier = 0.00;
    const double StatsPriority::sixthMultiplier = 0.00;
}