#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <sstream>
#include <iomanip>
#include <map>

namespace FFBE
{

    class Item
    {

    #pragma region Enumerables & Enumerable Maps

    public:
        enum class ItemType : uint32_t
        {
            UNKNOWN_ITEM_TYPE   = 0 ,
            CONSUMABLE          = ( 1 << 0 ) ,
            EQUIPMENT           = ( 1 << 1 ) ,
            MICS                = ( 1 << 2 )
        };

        typedef std::map<ItemType , std::string> ItemTypeMap_t;
        static ItemTypeMap_t itemTypeMap;

    #pragma endregion

    #pragma region Data Members

    private:
        std::string _name;
        ItemType _itemType;

    #pragma endregion

    #pragma region Constructors & Desctructor

    public:
        Item();
        Item( std::string name , ItemType itemType );

        virtual ~Item() = default;

    #pragma endregion

    #pragma region Getters & Setters

    public:
        std::string getName() const;
        void setName( const std::string name );

        ItemType getItemType() const;
        void setItemType( const ItemType itemType );

    #pragma endregion

    #pragma region Public Methods

    public:
        virtual std::string ToString() = 0;

    #pragma endregion

    };

}

#endif // !ITEM_H