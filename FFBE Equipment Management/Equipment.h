#ifndef EQUIPMENT_H
#define EQUIPMENT_H

#include <map>
#include <string>
#include <sstream>

#include "Item.h"
#include "Stats.h"

namespace FFBE
{

    class Equipment : public Item
    {

    #pragma region Enumerables & Enumerable Maps
            
    public:
        enum class EquipmentType
        {
            UNKNOWN_EQUIPMENT_TYPE = 0,
            HAND_EQUIPMENT  = ( 1 << 0 ) ,
            HEAD_EQUIPMENT  = ( 1 << 1 ) ,
            BODY_EQUIPMENT  = ( 1 << 2 ) ,
            ACCESSORY       = ( 1 << 3 )
        };

        typedef std::map<EquipmentType , std::string> EquipmentTypeMap_t;
        static EquipmentTypeMap_t equipmentTypeMap;

    #pragma endregion

    #pragma region Data Members

    private:
        EquipmentType _equipmentType;
        Stats _stats;

    #pragma endregion

    #pragma region Constructors & Destructor

    public:
        Equipment();
        Equipment( 
            std::string name , EquipmentType type ,
            int hp , int mp , int atk , int mag , int def , int spr );
        Equipment( const Equipment& newEquipment );

        virtual ~Equipment() = default;

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    private:
        void setEquipmentInfo(
            std::string name = "Unknown Equipment" ,
            EquipmentType type = EquipmentType::UNKNOWN_EQUIPMENT_TYPE
        );

    #pragma endregion

    #pragma region Getters & Setters

    public:
        EquipmentType getEquipmentType() const ;
        void setEquipmentType( const EquipmentType equipmentType );

        Stats getStats() const;
        void setStats( 
            const int hp = 0  , const int mp = 0 ,
            const int atk = 0 , const int mag = 0 ,
            const int def = 0 , const int spr = 0 );
        void setStats( const Stats stats );

    #pragma endregion

    #pragma region Overloading Operators

    public:
        Equipment& operator=( const Equipment& rhs );

    #pragma endregion

    #pragma region Public Methods

    public:
        Stats CalculateStatsDifference( const Equipment& newEquipment );
        virtual std::string GetEquipmentDetail();
        virtual std::string ToString() override;

    #pragma endregion

    };

}

#endif // !EQUIPMENT_H