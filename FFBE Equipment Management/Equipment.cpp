#include "Equipment.h"

namespace FFBE
{

    #pragma region Enumerable Maps

    Equipment::EquipmentTypeMap_t Equipment::equipmentTypeMap =
    {
        { EquipmentType::UNKNOWN_EQUIPMENT_TYPE , "Unknown" },
        { EquipmentType::HAND_EQUIPMENT     ,"Hand" },
        { EquipmentType::HEAD_EQUIPMENT     ,"Head" },
        { EquipmentType::BODY_EQUIPMENT     ,"Body" },
        { EquipmentType::ACCESSORY          , "Accessory" }
    };

    #pragma endregion

    #pragma region Constructors & Destructor

    Equipment::Equipment()
    {
        setItemType( ItemType::EQUIPMENT );
        setEquipmentInfo();
    }

    Equipment::Equipment( 
        std::string name , EquipmentType type ,
        int hp , int mp , int atk , int mag , int def , int spr )
    {
        setItemType( ItemType::EQUIPMENT );
        setEquipmentInfo( name , type );
        setStats( hp , mp , atk , mag , def , spr );
    }

    Equipment::Equipment( const Equipment & newEquipment )
    {
        *this = newEquipment;
    }

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    void Equipment::setEquipmentInfo( std::string name , EquipmentType type )
    {
        setName( name );
        setEquipmentType( type );
    }

    #pragma endregion

    #pragma region Getters & Setters

    Equipment::EquipmentType Equipment::getEquipmentType() const
    {
        return _equipmentType;
    }

    void Equipment::setEquipmentType( const EquipmentType equipmentType )
    {
        _equipmentType = equipmentType;
    }

    Stats Equipment::getStats() const
    {
        return _stats;
    }

    void Equipment::setStats(
        const int hp , const int mp ,
        const int atk , const int mag ,
        const int def , const int spr )
    {
        _stats.SetStats( hp , mp , atk , mag , def , spr );
    }

    void Equipment::setStats( const Stats stats )
    {
        _stats = stats;
    }

    #pragma endregion

    #pragma region Overloading Operator

    Equipment & Equipment::operator=( const Equipment & rhs )
    {
        if( this != &rhs )
        {
            setName( rhs.getName() );
            setEquipmentType( rhs.getEquipmentType() );
            setStats( rhs.getStats() );
        }

        return *this;
    }

    #pragma endregion

    #pragma region Public Methods

    Stats Equipment::CalculateStatsDifference( const Equipment & newEquipment )
    {
        Stats statsDifference;
        statsDifference.hp = _stats.hp - newEquipment.getStats().hp;
        statsDifference.mp = _stats.mp - newEquipment.getStats().mp;
        statsDifference.atk = _stats.atk - newEquipment.getStats().atk;
        statsDifference.mag = _stats.mag - newEquipment.getStats().mag;
        statsDifference.def = _stats.def - newEquipment.getStats().def;
        statsDifference.spr = _stats.spr - newEquipment.getStats().spr;

        return statsDifference;
    }

    std::string Equipment::GetEquipmentDetail()
    {
        std::stringstream ss;
        ss << std::setiosflags( std::ios::left );
        ss << std::setw( 32 ) << Equipment::ToString();
        ss << std::setw( 20 ) << equipmentTypeMap[ _equipmentType ];
        ss << getStats().ToString();
        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    std::string Equipment::ToString()
    {
        return getName();
    }

    #pragma endregion

}