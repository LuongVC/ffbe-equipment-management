#include "CharacterList.h"

namespace FFBE
{

    #pragma region Constructors & Destructor

    CharacterList::CharacterList()
    {
    }

    CharacterList::~CharacterList()
    {
    }

    #pragma endregion

    #pragma region Setters & Getters

    std::string CharacterList::getFilePath() const
    {
        return _filePath;
    }

    void CharacterList::setFilePath( const std::string & filePath )
    {
        _filePath = filePath;
    }

    #pragma endregion

    #pragma region Character Methods

    void CharacterList::loadCharacterFromFile()
    {
        loadCharacterFromFile( _filePath );
    }

    void CharacterList::loadCharacterFromFile( const std::string & filepath )
    {
        _characterList.clear();

        appendCharacterFromFile( filepath );
    }

    void CharacterList::writeCharacterToFile( const std::string & filepath )
    {
        std::cout << "Generating JSON object..." << std::endl;

        std::stringstream ss;
        ss << "{\"characterList\":[" << std::endl;

        auto numberOfCharactersConverted = 0;
        for( auto character : _characterList )
        {
            if( numberOfCharactersConverted > 0 )
            {
                ss << ",\n";
            }

            ss << "\t{" <<
                "\"name\":\"" << character->getName() << "\"" <<
                ",\"canDualWield\":" << (character->CanDualWield() ? "true" : "false") <<
                ",\"typeFormat\":\"uint\"" <<
                ",\"roleType\":" << uint32_t( character->getRoleType() ) <<
                ",\"weaponEquipmentType\":" << uint32_t( character->getWeaponType() ) <<
                ",\"shieldEquipmentType\":" << uint32_t( character->getShieldType() ) <<
                ",\"headEquipmentType\":" << uint32_t( character->getHeadEquipmentType() ) <<
                ",\"bodyEquipmentType\":" << uint32_t( character->getBodyEquipmentType() ) <<
                "}";

            numberOfCharactersConverted++;
        }

        ss << "\n]}" << std::endl;

        rapidjson::Document d;
        d.Parse( ss.str().c_str() );

        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer( buffer );
        d.Accept( writer );


        std::cout << "Creating JSON file: \"" << filepath << "\"" << std::endl;
        std::fstream outputFileStream( filepath , std::ios::out );
        if( outputFileStream.is_open() )
        {
            outputFileStream << ss.str() << std::endl;
        }
        outputFileStream.close();

        std::cout << "JSON File Created." << std::endl;
    }

    void CharacterList::appendCharacterFromFile( const std::string & filepath )
    {
        // Open the file
        FILE* inputFile;
        errno_t err = fopen_s( &inputFile , filepath.c_str() , "rb" );

        // Check for errors
        if( err != 0 )
        {
            // Error was found
            std::cout << "Cannot open file: " << filepath << std::endl;
        }
        else
        {
            // No Error was found
            std::cout << "Loading Characters from file: \"" << 
                filepath << "\"" << std::endl;

            // Create the buffer and read the file into the buffer
            char readBuffer[ 65536 ];
            rapidjson::FileReadStream inputStream( inputFile ,
                readBuffer , sizeof( readBuffer ) );

            // Create the JSON document and parse the input stream
            rapidjson::Document document;
            document.ParseStream( inputStream );

            // Close the file
            fclose( inputFile );

            const rapidjson::Value& characterList = document[ "characterList" ];
            assert( characterList.IsArray() );
            for( auto& character : characterList.GetArray() )
            {
                // Create the character
                std::shared_ptr<Character> newCharacter = std::make_shared<Character>();

                assert( character.HasMember( "name" ) );
                assert( character[ "name" ].IsString() );
                auto name = character[ "name" ].GetString();
                newCharacter->setName( name );

                assert( character.HasMember( "canDualWield" ) );
                assert( character[ "canDualWield" ].IsBool() );
                auto bCanDualWield = character[ "canDualWield" ].GetBool();
                newCharacter->CanDualWield( bCanDualWield );

                assert( character.HasMember( "typeFormat" ) );
                assert( character[ "typeFormat" ].IsString() );
                std::string typeFormat = character[ "typeFormat" ].GetString();
                std::transform( typeFormat.begin() , typeFormat.end() , typeFormat.begin() , ::tolower );

                if( typeFormat.compare( "uint" ) == 0 )
                {
                    assert( character.HasMember( "roleType" ) );
                    assert( character[ "roleType" ].IsUint() );
                    auto roleType = character[ "roleType" ].GetUint();
                    newCharacter->setRoleType( static_cast<Character::RoleType>( roleType ) );

                    assert( character.HasMember( "weaponType" ) );
                    assert( character[ "weaponType" ].IsUint() );
                    auto weaponType = character[ "weaponType" ].GetUint();
                    newCharacter->setWeaponType( static_cast<HandEquipment::WeaponType>( weaponType ) );

                    assert( character.HasMember( "shieldType" ) );
                    assert( character[ "shieldType" ].IsUint() );
                    auto shieldType = character[ "shieldType" ].GetUint();
                    newCharacter->setShieldType( static_cast<HandEquipment::ShieldType>( shieldType ) );

                    assert( character.HasMember( "headEquipmentType" ) );
                    assert( character[ "headEquipmentType" ].IsUint() );
                    auto headEquipmentType = character[ "headEquipmentType" ].GetUint();
                    newCharacter->setHeadEquipmentType( static_cast<HeadEquipment::HeadEquipmentType>( headEquipmentType ) );

                    assert( character.HasMember( "bodyEquipmentType" ) );
                    assert( character[ "bodyEquipmentType" ].IsUint() );
                    auto bodyEquipmentType = character[ "bodyEquipmentType" ].GetUint();
                    newCharacter->setBodyEquipmentType( static_cast<BodyEquipment::BodyEquipmentType>( bodyEquipmentType ) );
                }
                else if( typeFormat.compare( "string" ) == 0 )
                {
                    assert( character.HasMember( "roleType" ) );
                    assert( character[ "roleType" ].IsString() );
                    auto roleType = character[ "roleType" ].GetString();
                    newCharacter->setRoleType( roleType );

                    assert( character.HasMember( "weaponType" ) );
                    assert( character[ "weaponType" ].IsString() );
                    auto weaponType = character[ "weaponType" ].GetString();
                    newCharacter->setWeaponType( weaponType );

                    assert( character.HasMember( "shieldType" ) );
                    assert( character[ "shieldType" ].IsString() );
                    auto shieldType = character[ "shieldType" ].GetString();
                    newCharacter->setShieldType( shieldType );

                    assert( character.HasMember( "headEquipmentType" ) );
                    assert( character[ "headEquipmentType" ].IsString() );
                    auto headEquipmentType = character[ "headEquipmentType" ].GetString();
                    newCharacter->setHeadEquipmentType( headEquipmentType );

                    assert( character.HasMember( "bodyEquipmentType" ) );
                    assert( character[ "bodyEquipmentType" ].IsString() );
                    auto bodyEquipmentType = character[ "bodyEquipmentType" ].GetString();
                    newCharacter->setBodyEquipmentType( bodyEquipmentType );
                }

                // Set equipment optimizer base on role
                std::shared_ptr<EquipmentOptimizer> equipmentOptimizer;
                switch( newCharacter->getRoleType() )
                {
                    case Character::RoleType::MELEE:
                        equipmentOptimizer = std::make_shared<MeleeEquipmentOptimizer>();
                        break;
                    case Character::RoleType::BERSERK:
                        equipmentOptimizer = std::make_shared<BerserkEquipmentOptimizer>();
                        break;
                    case Character::RoleType::TANK:
                        equipmentOptimizer = std::make_shared<TankEquipmentOptimizer>();
                        break;
                    case Character::RoleType::RANGE:
                        equipmentOptimizer = std::make_shared<RangeEquipmentOptimizer>();
                        break;
                    case Character::RoleType::MAGE:
                        equipmentOptimizer = std::make_shared<MageEquipmentOptimizer>();
                        break;
                    case Character::RoleType::SUPPORT:
                        equipmentOptimizer = std::make_shared<SupportEquipmentOptimizer>();
                        break;
                    case Character::RoleType::HEALER:
                        equipmentOptimizer = std::make_shared<HealerEquipmentOptimizer>();
                        break;
                    default:
                        equipmentOptimizer = std::make_shared<MeleeEquipmentOptimizer>();
                        break;
                }
                newCharacter->setEquipmentOptimizer( equipmentOptimizer );

                _characterList.push_back( newCharacter );
            }

            std::cout << "Loading character list from file completed." << std::endl;
        }
    }

    void CharacterList::addCharacter( const std::shared_ptr<Character>& newCharacter )
    {
        _characterList.push_back( newCharacter );
    }

    #pragma endregion

    #pragma region Print Methods

    void CharacterList::printCharacterDetail( int index )
    {
        if( index >= 0 && index < static_cast<int>( _characterList.size() ) )
        {
            std::cout << _characterList.at( index )->getCharacterInfo();
        }
    }

    void CharacterList::printAllCharacterDetail()
    {
        std::for_each( _characterList.begin() , _characterList.end() , []( const std::shared_ptr<Character> character )
        {
            std::cout << character->getCharacterInfo();
        } );
    }

    void CharacterList::printAllEquipmentUsed()
    {
        std::cout <<
            "\n--------------------------------------------------\n" <<
            "Tallying All " << _characterList.size() << " Characters' Equipments\n" <<
            "--------------------------------------------------" << std::endl;

        auto equipmentList = std::move( tallyAllCharactersEquipment() );
        for( auto pair : equipmentList )
        {
            std::cout << pair.first << ": " << pair.second << std::endl;
        }

        std::cout << std::endl;
    }

    std::map<std::string , int> CharacterList::tallyAllCharactersEquipment()
    {
        std::map<std::string , int> equipmentList = std::map<std::string , int>();

        std::for_each(
            _characterList.begin() , _characterList.end() ,
            [ & ]( const std::shared_ptr<Character>& character )
        {
            tallyEquipmentList( character->getRHandEquipment() , equipmentList );
            tallyEquipmentList( character->getLHandEquipment() , equipmentList );
            tallyEquipmentList( character->getHeadEquipment() , equipmentList );
            tallyEquipmentList( character->getBodyEquipment() , equipmentList );
            tallyEquipmentList( character->getAccessory1Equipment() , equipmentList );
            tallyEquipmentList( character->getAccessory2Equipment() , equipmentList );
        } );

        return equipmentList;
    }

    void CharacterList::tallyEquipmentList( 
        const std::shared_ptr<Equipment>& equipment ,
        std::map<std::string , int>& equipmentList )
    {
        if( equipment->getName().compare( "None" ) != 0 )
        {
            auto result = equipmentList.find( equipment->getName() );
            if( result != equipmentList.end() )
            {
                result->second++;
            }
            else
            {
                equipmentList.insert( std::pair<std::string , unsigned int>(
                    equipment->getName() , 1 ) );
            }
        }
    }

    #pragma endregion

    #pragma region Optimize Equipment Methods

    void CharacterList::OptimizeRHandEquipment( const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        std::for_each( _characterList.begin() , _characterList.end() , [&]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeRHandEquipment( equipmentList );
        } );
    }

    void CharacterList::OptimizeLHandEquipment( const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        std::for_each( _characterList.begin() , _characterList.end() , [ & ]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeLHandEquipment( equipmentList );
        } );
    }

    void CharacterList::OptimizeHeadEquipment( const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        std::for_each( _characterList.begin() , _characterList.end() , [ & ]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeHeadEquipment( equipmentList );
        } );
    }

    void CharacterList::OptimizeBodyEquipment( const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        std::for_each( _characterList.begin() , _characterList.end() , [ & ]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeBodyEquipment( equipmentList );
        } );
    }

    void CharacterList::OptimizeAccessory1Equipment( const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        std::for_each( _characterList.begin() , _characterList.end() , [ & ]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeAccessory1Equipment( equipmentList );
        } );
    }

    void CharacterList::OptimizeAccessory2Equipment( const std::list<std::shared_ptr<Equipment>>& equipmentList)
    {
        std::for_each( _characterList.begin() , _characterList.end() , [ & ]( const std::shared_ptr<Character>& character )
        {
            character->OptimizeAccessory2Equipment( equipmentList );
        } );
    }
    
    #pragma endregion

}