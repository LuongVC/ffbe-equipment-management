#ifndef CHARACTERLIST_H
#define CHARACTERLIST_H

#include <memory>
#include <vector>
#include <string>
#include <cstring>
#include <fstream>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/filereadstream.h"

#include "Character.h"
#include "EquipmentOptimizer.h"

namespace FFBE
{

    class CharacterList
    {

    #pragma region Data Members

    private:
        std::string _filePath;
        std::vector<std::shared_ptr<Character>> _characterList;

    #pragma endregion

    #pragma region Constructors & Destructor

    public:
        CharacterList();

        virtual ~CharacterList();

    #pragma endregion

    #pragma region Setters & Getters

    public:
        std::string getFilePath() const;
        void setFilePath( const std::string& filePath );

    #pragma endregion

    #pragma region Character Methods

    public:
        void loadCharacterFromFile();
        void loadCharacterFromFile( const std::string& filepath );
        void writeCharacterToFile( const std::string& filepath );
        void appendCharacterFromFile( const std::string& filepath );
        void addCharacter( const std::shared_ptr<Character>& newCharacter );

    #pragma endregion

    #pragma region Print Methods

    public:
        void printCharacterDetail( int index );
        void printAllCharacterDetail();
        void printAllEquipmentUsed();
        std::map<std::string , int> tallyAllCharactersEquipment();

    private:
        void tallyEquipmentList( 
            const std::shared_ptr<Equipment>& equipment , 
            std::map<std::string , int>& equipmentList );

    #pragma endregion

    #pragma region Optimize Equipment Methods

    public:
        void OptimizeRHandEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeLHandEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeHeadEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeBodyEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeAccessory1Equipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeAccessory2Equipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );

    #pragma endregion

    };

}

#endif // !CHARACTERLIST_H