#include "Item.h"

namespace FFBE
{

    #pragma region Enumerable Maps

    Item::ItemTypeMap_t Item::itemTypeMap =
    {
        { ItemType::UNKNOWN_ITEM_TYPE   , "Unknown" },
        { ItemType::CONSUMABLE  , "Consumable" },
        { ItemType::EQUIPMENT   , "Equipment" },
        { ItemType::MICS        , "Misc" }
    };

    #pragma endregion

    #pragma region Constructors & Destructor

    Item::Item()
    {
        _name = "Unknown";
        _itemType = ItemType::UNKNOWN_ITEM_TYPE;
    }

    Item::Item( std::string name , ItemType itemType )
    {
        _name = name;
        _itemType = itemType;
    }

    #pragma endregion

    #pragma region Getters & Setters

    std::string Item::getName() const
    {
        return _name;
    }

    void Item::setName( const std::string name )
    {
        _name = name;
    }

    Item::ItemType Item::getItemType() const
    {
        return _itemType;
    }

    void Item::setItemType( const ItemType itemType )
    {
        _itemType = itemType;
    }

    #pragma endregion

    #pragma region Public Methods

    std::string Item::ToString()
    {
        std::stringstream ss;
        ss << "[" << itemTypeMap[ _itemType ] << "] " << _name;

        return ss.str();
    }

    #pragma endregion

}