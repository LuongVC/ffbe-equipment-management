#ifndef HEADEQUIPMENT_H
#define HEADEQUIPMENT_H

#include <string>
#include <map>
#include <limits>

#include "Equipment.h"
#include "InsensitiveCaseComparer.h"

namespace FFBE
{

    class HeadEquipment : public Equipment
    {

    #pragma region Enumerables & Enumerable Maps

    public:
        enum class HeadEquipmentType : uint32_t
        {
            NONE = 0 ,
            HAT = ( 1 << 0 ) ,
            HELMET = ( 1 << 1 )
        };

        typedef std::map<HeadEquipmentType , std::string> HeadEquipmentMap_t;
        static HeadEquipmentMap_t headEquipmentTypeMap;

        typedef std::map<std::string , HeadEquipmentType , InsensitiveCaseCompare> HeadEquipmentReverseMap_t;
        static HeadEquipmentReverseMap_t headEquipmentTypeReverseMap;

    #pragma endregion

    #pragma region Data Members

    private:
        HeadEquipmentType _headEquipmentType;

    #pragma endregion

    #pragma region Constructors & Destructor

    public:
        HeadEquipment();
        HeadEquipment(
            std::string name , HeadEquipmentType type ,
            int hp , int mp , int str , int mag , int def , int spr );
        HeadEquipment( const HeadEquipment& newHeadEquipment );

        virtual ~HeadEquipment() = default;

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    private:
        void setHeadEquipmentInfo( 
            std::string name = "None" , 
            HeadEquipmentType type = HeadEquipmentType::NONE );

    #pragma endregion

    #pragma region Getters & Setters

    public:
        HeadEquipmentType getHeadEquipmentType() const;
        void setHeadEquipmentType( HeadEquipmentType type );

    #pragma endregion

    #pragma region Overloading Operators

    public:
        HeadEquipment& operator=( const HeadEquipment& rhs );

    #pragma endregion

    #pragma region Public Methods

    public:
        std::string GetEquipmentDetail() override;
        std::string ToString() override;

    #pragma endregion

    };

    #pragma region Enumerable Overloading Operators

    HeadEquipment::HeadEquipmentType operator|(
        const HeadEquipment::HeadEquipmentType& lhs ,
        const HeadEquipment::HeadEquipmentType& rhs );

    HeadEquipment::HeadEquipmentType operator&(
        const HeadEquipment::HeadEquipmentType& lhs ,
        const HeadEquipment::HeadEquipmentType& rhs );

    #pragma endregion

}

#endif // !HEADEQUIPMENT_H