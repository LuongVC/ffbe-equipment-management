#ifndef OPTIMIZEEQUIPMENT_H
#define OPTIMIZEEQUIPMENT_H

#include <memory>
#include <list>

#include "StatsPriority.h"
#include "Equipment.h"
#include "EquipableFlags.h"

namespace FFBE
{

    class OptimizeEquipment
    {
    protected:
        StatsPriority _statsPriority;

    protected:
        double calculateEquipmentValue( const std::shared_ptr<Equipment>& newEquipment );
        double calculateStatValue(
            const std::shared_ptr<Equipment>& newEquipment ,
            const Stats::StatType& statType );

    public:
        virtual void optimizeEquipment(
            std::shared_ptr<Equipment>& equippedEquipment ,
            const EquipableFlags& equipableEquipmentFlag ,
            const std::list<std::shared_ptr<Equipment>>& newEquipmentList );

        virtual void optimizeEquipment(
            std::shared_ptr<Equipment>& equippedEquipment ,
            const EquipableFlags& equipableEquipmentFlag ,
            const std::shared_ptr<Equipment>& newEquipment );

        virtual bool canEquip(
            const std::shared_ptr<Equipment> equipment ,
            const EquipableFlags& equipableEquipmentFlag ) const = 0;
    };

}

#endif // !OPTIMIZEEQUIPMENT_H