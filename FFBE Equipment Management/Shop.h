#ifndef SHOP_H
#define SHOP_H

#include <memory>
#include <list>
#include <algorithm>
#include <iostream>

#include "HandEquipment.h"
#include "HeadEquipment.h"
#include "BodyEquipment.h"

namespace FFBE
{

    class Shop
    {

    #pragma region Data Members

    private:
        std::list<std::shared_ptr<Equipment>> _weaponList;
        std::list<std::shared_ptr<Equipment>> _shieldList;
        std::list<std::shared_ptr<Equipment>> _headwearList;
        std::list<std::shared_ptr<Equipment>> _bodywearList;
        std::list<std::shared_ptr<Equipment>> _accessoryList;

    #pragma endregion

    #pragma region Constructors & Destructors

    public:
        Shop();

        virtual ~Shop();

    #pragma endregion

    #pragma region Constructors & Destructors Helper Methods

    private:
        void PopulateWeaponList();
        void PopulateShieldList();
        void PopulateHeadwearList();
        void PopulateBodywearList();
        void PopulateAccessoryList();

    #pragma endregion

    #pragma region Setters & Getters

    public:
        std::list<std::shared_ptr<Equipment>>const& getWeaponList() const;
        void setWeaponList( std::list<std::shared_ptr<Equipment>>& equipmentList );

        std::list<std::shared_ptr<Equipment>>const& getShieldList() const;
        void setShieldList( std::list<std::shared_ptr<Equipment>>& equipmentList );

        std::list<std::shared_ptr<Equipment>>const& getHeadwearList() const;
        void setHeadwearList( std::list<std::shared_ptr<Equipment>>& equipmentList );

        std::list<std::shared_ptr<Equipment>>const& getBodywearList() const;
        void setBodywearList( std::list<std::shared_ptr<Equipment>>& equipmentList );

        std::list<std::shared_ptr<Equipment>>const& getAccessoryList() const;
        void setAccessoryList( std::list<std::shared_ptr<Equipment>>& equipmentList );

    #pragma endregion

    };

}

#endif // !SHOP_H