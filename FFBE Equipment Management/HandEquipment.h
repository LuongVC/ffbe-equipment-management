#ifndef HANDEQUIPMENT_H
#define HANDEQUIPMENT_H

#include <string>
#include <sstream>
#include <iomanip>
#include <map>
#include <limits>

#include "Equipment.h"
#include "InsensitiveCaseComparer.h"

namespace FFBE
{

    class HandEquipment : public Equipment
    {

    #pragma region Enumerables & Enumerable Maps

    public:
        enum class WeaponType : uint32_t
        {
            NONE                = 0 ,
            DAGGER              = ( 1 << 0 ) ,
            SWORD               = ( 1 << 1 ) ,
            GREAT_SWORD         = ( 1 << 2 ) ,
            KATANA              = ( 1 << 3 ) ,
            STAFF               = ( 1 << 4 ) ,
            ROD                 = ( 1 << 5 ) ,
            BOW                 = ( 1 << 6 ) ,
            AXE                 = ( 1 << 7 ) ,
            HAMMER              = ( 1 << 8 ) ,
            SPEAR               = ( 1 << 9 ) ,
            HARP                = ( 1 << 10 ) ,
            WHIP                = ( 1 << 11 ) ,
            THROWING_WEAPON     = ( 1 << 12 ) ,
            GUN                 = ( 1 << 13 ) ,
            MACE                = ( 1 << 14 ) ,
            FIST                = ( 1 << 15 )
        };
        enum class ShieldType : uint32_t
        {
            NONE = 0 ,
            LIGHT_SHIELD = ( 1 << 0 ) ,
            HEAVY_SHIELD = ( 1 << 1 )
        };
        enum class HandleType : uint32_t
        {
            NONE                = 0 ,
            ONEHANDED_EQUIPMENT = ( 1 << 0 ) ,
            TWOHANDED_EQUIPMENT = ( 1 << 1 )
        };

        typedef std::map<WeaponType , std::string> WeaponTypeMap_t;
        static WeaponTypeMap_t weaponTypeMap;

        typedef std::map<std::string , WeaponType , InsensitiveCaseCompare> WeaponTypeReverseMap_t;
        static WeaponTypeReverseMap_t weaponTypeReverseMap;

        typedef std::map<ShieldType , std::string> ShieldTypeMap_t;
        static ShieldTypeMap_t shieldTypeMap;

        typedef std::map<std::string , ShieldType , InsensitiveCaseCompare> ShieldTypeReverseMap_t;
        static ShieldTypeReverseMap_t shieldTypeReverseMap;

        typedef std::map<HandleType , std::string> HandleType_t;
        static HandleType_t handleTypeMap;

    #pragma endregion

    #pragma region Data Members

    public:
        WeaponType _weaponType;
        ShieldType _shieldType;
        HandleType _handleType;

    #pragma endregion

    #pragma region Constructors & Destructor

    public:
        HandEquipment();
        HandEquipment( 
            std::string name ,
            WeaponType type , HandleType handle ,
            int hp , int mp , int str , int mag , int def , int spr );
        HandEquipment(
            std::string name ,
            ShieldType type , HandleType handle ,
            int hp , int mp , int str , int mag , int def , int spr );
        HandEquipment( const HandEquipment& newHandEquipment );

        virtual ~HandEquipment() = default;

    #pragma endregion

    #pragma region Constructors & Destructors Helper Functions

    private:
        void setWeaponInfo(
            std::string name = "None" , 
            WeaponType weaponType = WeaponType::NONE ,
            HandleType handle = HandleType::NONE );

        void setShieldInfo(
            std::string name = "None" ,
            ShieldType shieldType = ShieldType::NONE ,
            HandleType handle = HandleType::NONE );

    #pragma endregion

    # pragma region Getters & Setters

    public:
        WeaponType getWeaponType() const;
        void setWeaponType( const WeaponType type );

        ShieldType getShieldType() const;
        void setShieldType( const ShieldType type );

        HandleType getHandleType() const;
        void setHandleType( const HandleType type );

    #pragma endregion

    #pragma region Overloading Operators

    public:
        HandEquipment& operator=( const HandEquipment& rhs );

    #pragma endregion

    #pragma region Public Methods

    public:
        std::string GetEquipmentDetail() override;
        std::string ToString() override;

    #pragma endregion

    };

    #pragma region Enumerable Overloading Operators

    HandEquipment::WeaponType operator|( 
        const HandEquipment::WeaponType& lhs , 
        const HandEquipment::WeaponType& rhs );

    HandEquipment::WeaponType operator&( 
        const HandEquipment::WeaponType& lhs , 
        const HandEquipment::WeaponType& rhs );

    HandEquipment::ShieldType operator|(
        const HandEquipment::ShieldType& lhs ,
        const HandEquipment::ShieldType& rhs );

    HandEquipment::ShieldType operator&(
        const HandEquipment::ShieldType& lhs ,
        const HandEquipment::ShieldType& rhs );

    #pragma endregion

}

#endif // !HANDEQUIPMENT_H