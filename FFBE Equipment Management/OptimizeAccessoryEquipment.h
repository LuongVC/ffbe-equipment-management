#ifndef OPTIMIZEACCESSORYEQUIPMENT_H
#define OPTIMIZEACCESSORYEQUIPMENT_H

#include "OptimizeEquipment.h"

namespace FFBE
{

    class OptimizeAccessoryEquipment : public OptimizeEquipment
    {

    #pragma region Constructors & Destructor

    public:
        OptimizeAccessoryEquipment(
            Stats::StatType first = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType second = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType third = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType fourth = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType fifth = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType sixth = Stats::StatType::UKNOWN_STAT_TYPE
        );

        virtual ~OptimizeAccessoryEquipment() { };

    #pragma endregion

    #pragma region Override Methods

    public:
        virtual bool canEquip(
            const std::shared_ptr<Equipment> equipment , 
            const EquipableFlags & equipableEquipmentFlag ) const override;

    #pragma endregion

    };

}

#endif // !OPTIMIZEACCESSORYEQUIPMENT_H