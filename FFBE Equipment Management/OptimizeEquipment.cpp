#include "OptimizeEquipment.h"

namespace FFBE
{

    double OptimizeEquipment::calculateEquipmentValue( const std::shared_ptr<Equipment>& newEquipment )
    {
        double hpValue = calculateStatValue( newEquipment , Stats::StatType::HP );
        double mpValue = calculateStatValue( newEquipment , Stats::StatType::MP );
        double atkValue = calculateStatValue( newEquipment , Stats::StatType::ATK );
        double magValue = calculateStatValue( newEquipment , Stats::StatType::MAG );
        double defValue = calculateStatValue( newEquipment , Stats::StatType::DEF );
        double sprValue = calculateStatValue( newEquipment , Stats::StatType::SPR );

        return ( hpValue  + mpValue +
                 atkValue + magValue + 
                 defValue + sprValue );
    }

    double OptimizeEquipment::calculateStatValue( const std::shared_ptr<Equipment>& newEquipment , const Stats::StatType & statType )
    {
        double statValue = 0.0;
        switch( statType )
        {
            case Stats::StatType::HP:
                statValue = newEquipment->getStats().hp;
                break;
            case Stats::StatType::MP:
                statValue = newEquipment->getStats().mp;
                break;
            case Stats::StatType::ATK:
                statValue = newEquipment->getStats().atk;
                break;
            case Stats::StatType::MAG:
                statValue = newEquipment->getStats().mag;
                break;
            case Stats::StatType::DEF:
                statValue = newEquipment->getStats().def;
                break;
            case Stats::StatType::SPR:
                statValue = newEquipment->getStats().spr;
                break;
        }

        double valueMultiplier = 0.0;
        if( statType == _statsPriority.first )
        {
            valueMultiplier = _statsPriority.firstMultiplier;
        }
        else if( statType == _statsPriority.second )
        {
            valueMultiplier = _statsPriority.secondMultiplier;
        }
        else if( statType == _statsPriority.third )
        {
            valueMultiplier = _statsPriority.thirdMultiplier;
        }
        else if( statType == _statsPriority.fourth )
        {
            valueMultiplier = _statsPriority.fourthMultiplier;
        }
        else if( statType == _statsPriority.fifth )
        {
            valueMultiplier = _statsPriority.fifthMultiplier;
        }
        else if( statType == _statsPriority.sixth )
        {
            valueMultiplier = _statsPriority.sixthMultiplier;
        }

        return ( statValue * valueMultiplier );
    }

    void OptimizeEquipment::optimizeEquipment(
        std::shared_ptr<Equipment>& equippedEquipment ,
        const EquipableFlags & equipableEquipmentFlag ,
        const std::list<std::shared_ptr<Equipment>>& newEquipmentList )
    {
        for( auto equipment : newEquipmentList )
        {
            optimizeEquipment(
                equippedEquipment ,
                equipableEquipmentFlag ,
                equipment );
        };
    }

    void OptimizeEquipment::optimizeEquipment(
        std::shared_ptr<Equipment>& equippedEquipment ,
        const EquipableFlags & equipableEquipmentFlag ,
        const std::shared_ptr<Equipment>& newEquipment )
    {
        if( canEquip( newEquipment , equipableEquipmentFlag ) )
        {
            auto currentEquipmentValue = calculateEquipmentValue( equippedEquipment );
            auto newEquipmentValue = calculateEquipmentValue( newEquipment );
            auto valueDifference = newEquipmentValue - currentEquipmentValue;

            if( valueDifference > 0 )
            {
                equippedEquipment = newEquipment;
            }
        }
    }

}