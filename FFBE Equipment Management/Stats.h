#ifndef STATS_H
#define STATS_H

#include <string>
#include <sstream>
#include <iomanip>

namespace FFBE
{

    struct Stats
    {

    #pragma region Enumerables

    public:
        enum class StatType
        {
            UKNOWN_STAT_TYPE = 0 ,
            HP = ( 1 << 0 ) ,
            MP = ( 1 << 1 ) ,
            ATK = ( 1 << 2 ) ,
            MAG = ( 1 << 3 ) ,
            DEF = ( 1 << 4 ) ,
            SPR = ( 1 << 5 )
        };

    #pragma endregion

    #pragma region Data Members

    public:
        int hp = 0;    int mp = 0;
        int atk = 0;    int mag = 0;
        int def = 0;    int spr = 0;

    #pragma endregion

    #pragma region Setters & Getters

    public:
        void SetStats( const int hp , const int mp ,
            const int atk , const int mag ,
            const int def , const int spr );
        void SetStats( const Stats& newStats );
        Stats GetStats();

    #pragma endregion

    #pragma region Overloading Operators

    public:
        Stats& operator=( const Stats& newStats );

    #pragma endregion

    #pragma region Public Methods

    public:
        std::string ToString();

    #pragma endregion

    };

}

#endif // !STATS_H