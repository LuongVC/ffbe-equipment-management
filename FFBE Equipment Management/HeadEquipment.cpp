#include "HeadEquipment.h"

namespace FFBE
{

    #pragma region Enumerable Maps

    HeadEquipment::HeadEquipmentMap_t HeadEquipment::headEquipmentTypeMap =
    {
        { HeadEquipmentType::NONE   , "None" },
        { HeadEquipmentType::HAT    , "Hat" },
        { HeadEquipmentType::HELMET , "Helmet" }
    };

    HeadEquipment::HeadEquipmentReverseMap_t HeadEquipment::headEquipmentTypeReverseMap =
    {
        { "None"    , HeadEquipmentType::NONE },
        { "Hat"     , HeadEquipmentType::HAT },
        { "Helmet"  , HeadEquipmentType::HELMET }
    };

    #pragma endregion

    #pragma region Constructors & Destructor

    HeadEquipment::HeadEquipment()
    {
        setEquipmentType( EquipmentType::HEAD_EQUIPMENT );
        setHeadEquipmentInfo();
    }

    HeadEquipment::HeadEquipment( 
        std::string name , HeadEquipmentType type , 
        int hp , int mp , int str , int mag , int def , int spr )
    {
        setEquipmentType( EquipmentType::HEAD_EQUIPMENT );
        setHeadEquipmentInfo( name , type );
        setStats( hp , mp , str , mag , def , spr );
    }

    HeadEquipment::HeadEquipment( const HeadEquipment & newHeadEquipment )
    {
        *this = newHeadEquipment;
    }

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    void HeadEquipment::setHeadEquipmentInfo( std::string name , HeadEquipmentType type )
    {
        setName( name );

        _headEquipmentType = type;
    }

    #pragma endregion

    #pragma region Getters & Setters

    HeadEquipment::HeadEquipmentType HeadEquipment::getHeadEquipmentType() const
    {
        return _headEquipmentType;
    }

    void HeadEquipment::setHeadEquipmentType( HeadEquipmentType type )
    {
        _headEquipmentType = type;
    }

    #pragma endregion

    #pragma region Overloading Operators

    HeadEquipment& HeadEquipment::operator=( const HeadEquipment& rhs )
    {
        if( this != &rhs )
        {
            setName( rhs.getName() );
            setStats( rhs.getStats() );

            _headEquipmentType = rhs.getHeadEquipmentType();
        }

        return *this;
    }

    #pragma endregion

    #pragma region Public Methods

    std::string HeadEquipment::GetEquipmentDetail()
    {
        std::stringstream ss;
        ss << std::setiosflags( std::ios::left );
        ss << std::setw( 32 ) << Equipment::ToString();
        ss << std::setw( 20 ) << headEquipmentTypeMap[ _headEquipmentType ];
        ss << getStats().ToString();
        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    std::string HeadEquipment::ToString()
    {
        return getName();
    }

    #pragma endregion

    #pragma region Enumerable Overloading Operators

    HeadEquipment::HeadEquipmentType operator|( 
        const HeadEquipment::HeadEquipmentType& lhs , 
        const HeadEquipment::HeadEquipmentType& rhs )
    {
        return static_cast<HeadEquipment::HeadEquipmentType>(
            ( uint32_t( lhs ) | uint32_t( rhs ) ) );
    }

    HeadEquipment::HeadEquipmentType operator&( 
        const HeadEquipment::HeadEquipmentType& lhs , 
        const HeadEquipment::HeadEquipmentType& rhs )
    {
        return static_cast<HeadEquipment::HeadEquipmentType>(
            ( uint32_t( lhs ) & uint32_t( rhs ) ) );
    }

    #pragma endregion

}