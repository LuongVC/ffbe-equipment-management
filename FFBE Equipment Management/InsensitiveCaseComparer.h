#ifndef INSENSITIVECASECOMPARER
#define INSENSITIVECASECOMPARER

#include <string>
#include <algorithm>

struct InsensitiveCaseCompare
{
    bool operator()( const std::string& lhs , const std::string& rhs ) const
    {
        std::string lhsString( lhs.length() , ' ' );
        std::string rhsString( rhs.length() , ' ' );
        std::transform( lhs.begin() , lhs.end() , lhsString.begin() , tolower );
        std::transform( rhs.begin() , rhs.end() , rhsString.begin() , tolower );

        return lhsString < rhsString;
    };
};



#endif // !INSENSITIVECASECOMPARER