#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include <sstream>
#include <cctype>       // std::isspace
#include <map>
#include <limits>       // uint32_t
#include <vector>
#include <algorithm>    // std::remove_if

#include "Equipment.h"

namespace FFBE
{
    #pragma region Prototypes

    template<class T , class M>
    static inline std::string ConvertEquipmentFlagToString( T flag , char delimiter , M map_t );

    template<class T , class RM>
    static inline T ConvertStringToEquipmentFlag( std::string strFlag , char delimiter , RM reverseMap_t );

    static inline void TrimLead( std::string& s );
    static inline void TrimTrail( std::string& s );
    static inline void Trim( std::string& text );

    #pragma endregion

    template<class T , class M>
    static inline std::string ConvertEquipmentFlagToString( T flag , char delimiter , M map_t )
    {
        std::stringstream ss;
        int bitSize = std::numeric_limits<uint32_t>::digits;
        uint32_t flagIndex = 0;

        if( flag != (T)flagIndex )
        {
            flagIndex = 1;
            for( int i = 1 ; i < bitSize ; i++ )
            {
                auto currentWeaponType = (T)flagIndex;
                if( uint32_t( flag & currentWeaponType ) )
                {
                    if( ss.tellp() > 0 )
                    {
                        ss << delimiter;
                    }
                    ss << map_t[ (T)flagIndex ];
                }
                flagIndex = flagIndex << 1;
            }
        }
        else
        {
            ss << "None";
        }

        return ss.str();
    }

    template<class T , class RM>
    static inline T ConvertStringToEquipmentFlag( std::string strFlag , char delimiter , RM reverseMap_t )
    {
        T equipmentFlag = static_cast<T>(0);
        auto flags = std::move( Split( strFlag , delimiter ) );

        std::for_each( flags.begin() , flags.end() , [&]( const std::string& equipment )
        {
            auto pair = reverseMap_t.find( equipment );
            if( pair != reverseMap_t.end() )
            {
                auto flag = (T)pair->second;
                equipmentFlag = equipmentFlag | flag;
            }
        } );

        return equipmentFlag;
    }

    static inline std::vector<std::string> Split( const std::string& s , char delimiter )
    {
        std::vector<std::string> tokens;
        int start = 0;
        int end = 0;

        std::for_each( s.begin() , s.end() , [&]( char c )
        {
            if( c == delimiter )
            {
                std::string token = s.substr( start , end - start );
                Trim( token );
                tokens.push_back( token );
                start = end + 1;
            }
            end++;
        } );

        std::string token = s.substr( start , s.length() - start );
        Trim( token );
        tokens.push_back( token );

        return tokens;
    }

    static inline void TrimLead( std::string& s )
    {
        auto f = []( unsigned char const c ) { return std::isspace( c ); };
        s.erase( s.begin() , std::find_if_not( s.begin() , s.end() , f ) );
    }

    static inline void TrimTrail( std::string& s )
    {
        auto f = []( unsigned char const c ) { return std::isspace( c ); };
        s.erase( std::find_if_not( s.rbegin() , s.rend() , f ).base() , s.end() );
    }

    static inline void Trim( std::string& text )
    {
        TrimLead( text );
        TrimTrail( text );
    }
}

#endif // !UTILITY_H