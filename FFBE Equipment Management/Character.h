#ifndef CHARACTER_H
#define CHARACTER_H

#include <memory>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <list>
#include <algorithm>
#include <limits>

#include "EquipableFlags.h"
#include "EquipmentOptimizer.h"
#include "Utility.h"
#include "InsensitiveCaseComparer.h"


namespace FFBE
{

    class Character
    {

    #pragma region Enumerables & Enumerable Maps

    public:
        enum class RoleType : uint32_t
        {
            NONE        = 0 ,
            MELEE       = ( 1 << 0 ) ,
            BERSERK     = ( 1 << 1 ) ,
            TANK        = ( 1 << 2 ) ,
            RANGE       = ( 1 << 3 ) ,
            MAGE        = ( 1 << 4 ) ,
            SUPPORT     = ( 1 << 5 ) ,
            HEALER      = ( 1 << 6 )
        };

        typedef std::map<RoleType , std::string> RoleTypeMap_t;
        static RoleTypeMap_t roleTypeMap;

        typedef std::map<std::string , RoleType , InsensitiveCaseCompare> RoleTypeReverseMap_t;
        static RoleTypeReverseMap_t roleTypeReverseMap;

    #pragma endregion
    
    #pragma region Data Members

    private:
        std::string _name;
        RoleType _roleType;
        bool _bCanDualWield;
        EquipableFlags _equipableFlags;

        std::shared_ptr<Equipment> _rightHand;
        std::shared_ptr<Equipment> _leftHand;
        std::shared_ptr<Equipment> _head;
        std::shared_ptr<Equipment> _body;
        std::shared_ptr<Equipment> _accessory1;
        std::shared_ptr<Equipment> _accessory2;

        std::shared_ptr<EquipmentOptimizer> _equipmentOptimizer;

    #pragma endregion

    #pragma region Constructors & Destructors

    public:
        Character();
        Character( std::string name );
        Character( std::string name ,  bool canDualWield ,
            HandEquipment::WeaponType weaponEquipmentFlag ,
            HandEquipment::ShieldType shieldEquipmentFlag ,
            HeadEquipment::HeadEquipmentType headEquipmentFlag ,
            BodyEquipment::BodyEquipmentType bodyEquipmentFlag );

        virtual ~Character();

    #pragma endregion

    #pragma region Constructors & Destructors Helper Methods

    private:
        void initializeEquipmentFlags();
        void instantiateEquipments();

    #pragma endregion

    #pragma region Getters & Setters

    public:
        std::string getName() const;
        void setName( const std::string name );

        RoleType getRoleType() const;
        void setRoleType( const RoleType roleType );
        void setRoleType( const std::string& flag , char delimiter = ',' );

        bool CanDualWield() const;
        void CanDualWield( const bool canDualWield );

        HandEquipment::WeaponType getWeaponType() const;
        void setWeaponType( const HandEquipment::WeaponType flag );
        void setWeaponType( const std::string& flag , char delimiter = ',' );

        HandEquipment::ShieldType getShieldType() const;
        void setShieldType( const HandEquipment::ShieldType flag );
        void setShieldType( const std::string& flag , char delimiter = ',' );

        HeadEquipment::HeadEquipmentType getHeadEquipmentType() const;
        void setHeadEquipmentType( const HeadEquipment::HeadEquipmentType flag );
        void setHeadEquipmentType( const std::string& flag , char delimiter = ',' );

        BodyEquipment::BodyEquipmentType getBodyEquipmentType() const;
        void setBodyEquipmentType( const BodyEquipment::BodyEquipmentType flag );
        void setBodyEquipmentType( const std::string& flag , char delimiter = ',' );

        void setEquipmentOptimizer( const std::shared_ptr<EquipmentOptimizer>& equipmentOptimizer );

    #pragma endregion

    #pragma region Equipment Methods

    public:
        const std::shared_ptr<Equipment>& getRHandEquipment() const;
        void equipRHand( const std::shared_ptr<Equipment>& equipment );

        const std::shared_ptr<Equipment>& getLHandEquipment() const;
        void equipLHand( const std::shared_ptr<Equipment>& equipment );

        const std::shared_ptr<Equipment>& getHeadEquipment() const;
        void equipHead( const std::shared_ptr<Equipment>& equipment );

        const std::shared_ptr<Equipment>& getBodyEquipment() const;
        void equipBody( const std::shared_ptr<Equipment>& equipment );

        const std::shared_ptr<Equipment>& getAccessory1Equipment() const;
        void equipAccessory1( const std::shared_ptr<Equipment>& equipment );

        const std::shared_ptr<Equipment>& getAccessory2Equipment() const;
        void equipAccessory2( const std::shared_ptr<Equipment>& equipment );

        bool CanEquip( 
            const std::shared_ptr<Equipment>& equipment , 
            const Equipment::EquipmentType& type );

    #pragma endregion

    #pragma region Optimize Equipment Methods

        void OptimizeRHandEquipment( 
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeLHandEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeHeadEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeBodyEquipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeAccessory1Equipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );
        void OptimizeAccessory2Equipment(
            const std::list<std::shared_ptr<Equipment>>& equipmentList );

    #pragma endregion

    #pragma region Public Methods

    public:
        std::string ToString();
        std::string getCharacterInfo() const;

    #pragma endregion

    };

    #pragma region Enumerable Overloading Operators

    Character::RoleType operator|(
        const Character::RoleType& lhs ,
        const Character::RoleType& rhs );

    Character::RoleType operator&(
        const Character::RoleType& lhs ,
        const Character::RoleType& rhs );

    #pragma endregion

}

#endif // !CHARACTER_H