#pragma once

#include "OptimizeEquipment.h"

namespace FFBE
{

    class OptimizeShieldEquipment : public OptimizeEquipment
    {

    #pragma region Constructors & Destructor

    public:
        OptimizeShieldEquipment(
            Stats::StatType first = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType second = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType third = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType fourth = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType fifth = Stats::StatType::UKNOWN_STAT_TYPE ,
            Stats::StatType sixth = Stats::StatType::UKNOWN_STAT_TYPE
        );

        virtual ~OptimizeShieldEquipment() { };

    #pragma endregion

    #pragma region Override Methods

    public:
        virtual bool canEquip( 
            const std::shared_ptr<Equipment> equipment , 
            const EquipableFlags & equipableEquipmentFlag ) const override;

    #pragma endregion

    };

}