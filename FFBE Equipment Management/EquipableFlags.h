#ifndef EQUIPABLEFLAGS_H
#define EQUIPABLEFLAGS_H

#include "HandEquipment.h"
#include "HeadEquipment.h"
#include "BodyEquipment.h"

namespace FFBE
{

    struct EquipableFlags
    {
        HandEquipment::WeaponType Weapon;
        HandEquipment::ShieldType Shield;
        HeadEquipment::HeadEquipmentType Head;
        BodyEquipment::BodyEquipmentType Body;
    };

}

#endif // !EQUIPABLEFLAGS_H