#include "Character.h"

namespace FFBE
{

    #pragma region Enumerable Maps

    Character::RoleTypeMap_t Character::roleTypeMap =
    {
        { RoleType::NONE        , "None" } ,
        { RoleType::MELEE       , "Melee" } ,
        { RoleType::BERSERK     , "Berserk" } ,
        { RoleType::TANK        , "Tank" } ,
        { RoleType::RANGE       , "Range" } ,
        { RoleType::MAGE        , "Mage" } ,
        { RoleType::SUPPORT     , "Support" } ,
        { RoleType::HEALER      , "Healer" }
    };

    Character::RoleTypeReverseMap_t Character::roleTypeReverseMap =
    {
        { "None"                , RoleType::NONE } ,
        { "Melee"               , RoleType::MELEE } ,
        { "Berserk"             , RoleType::BERSERK } ,
        { "Tank"                , RoleType::TANK } ,
        { "Range"               , RoleType::RANGE } ,
        { "Mage"                , RoleType::MAGE } ,
        { "Support"             , RoleType::SUPPORT } ,
        { "Healer"              , RoleType::HEALER }
    };

    #pragma endregion

    #pragma region Constructors & Destructors

    Character::Character()
    {
        _name = "Unknown";
        _bCanDualWield = false;
        
        initializeEquipmentFlags();
        instantiateEquipments();
    }

    Character::Character( std::string name )
    {
        _name = name;
        _bCanDualWield = false;

        initializeEquipmentFlags();
        instantiateEquipments();
    }

    Character::Character( std::string name , bool canDualWield ,
        HandEquipment::WeaponType weaponEquipmentFlag ,
        HandEquipment::ShieldType shieldEquipmentFlag ,
        HeadEquipment::HeadEquipmentType headEquipmentFlag ,
        BodyEquipment::BodyEquipmentType bodyEquipmentFlag )
    {
        _name = name;
        _bCanDualWield = false;
        _equipableFlags.Weapon = weaponEquipmentFlag;
        _equipableFlags.Shield = shieldEquipmentFlag;
        _equipableFlags.Head = headEquipmentFlag;
        _equipableFlags.Body = bodyEquipmentFlag;

        instantiateEquipments();
    }

    Character::~Character()
    {
    }

    #pragma endregion

    #pragma region Constructors & Destructors Helper Methods

    void Character::initializeEquipmentFlags()
    {
        _equipableFlags.Weapon = HandEquipment::WeaponType::NONE;
        _equipableFlags.Shield = HandEquipment::ShieldType::NONE;
        _equipableFlags.Head = HeadEquipment::HeadEquipmentType::NONE;
        _equipableFlags.Body = BodyEquipment::BodyEquipmentType::NONE;
    }

    void Character::instantiateEquipments()
    {
        if( _rightHand == nullptr )     _rightHand = std::make_shared<HandEquipment>();
        if( _leftHand == nullptr )      _leftHand = std::make_shared<HandEquipment>();
        if( _head == nullptr )          _head = std::make_shared<HeadEquipment>();
        if( _body == nullptr )          _body = std::make_shared<BodyEquipment>();
        if( _accessory1 == nullptr )    _accessory1 = std::make_shared<Equipment>();
        if( _accessory2 == nullptr )    _accessory2 = std::make_shared<Equipment>();
    }

    #pragma endregion

    #pragma region Getters & Setters

    std::string Character::getName() const
    {
        return _name;
    }

    void Character::setName( const std::string name )
    {
        _name = name;
    }

    Character::RoleType Character::getRoleType() const
    {
        return _roleType;
    }

    void Character::setRoleType( const RoleType roleType )
    {
        _roleType = roleType;
    }

    void Character::setRoleType( const std::string & flag , char delimiter )
    {
        _roleType = FFBE::ConvertStringToEquipmentFlag<
            FFBE::Character::RoleType ,
            FFBE::Character::RoleTypeReverseMap_t >(
                flag , delimiter ,
                FFBE::Character::roleTypeReverseMap );
    }

    bool Character::CanDualWield() const
    {
        return _bCanDualWield;
    }

    void Character::CanDualWield( const bool canDualWield )
    {
        _bCanDualWield = canDualWield;
    }

    HandEquipment::WeaponType Character::getWeaponType() const
    {
        return _equipableFlags.Weapon;
    }

    void Character::setWeaponType( const HandEquipment::WeaponType flag )
    {
        _equipableFlags.Weapon = flag;
    }

    void Character::setWeaponType( const std::string& flag , char delimiter )
    {
        _equipableFlags.Weapon = FFBE::ConvertStringToEquipmentFlag<
            FFBE::HandEquipment::WeaponType ,
            FFBE::HandEquipment::WeaponTypeReverseMap_t >(
                flag , delimiter ,
                FFBE::HandEquipment::weaponTypeReverseMap );
    }

    HandEquipment::ShieldType Character::getShieldType() const
    {
        return _equipableFlags.Shield;
    }

    void Character::setShieldType( const HandEquipment::ShieldType flag )
    {
        _equipableFlags.Shield = flag;
    }

    void Character::setShieldType( const std::string & flag , char delimiter )
    {
        _equipableFlags.Shield = FFBE::ConvertStringToEquipmentFlag<
            FFBE::HandEquipment::ShieldType ,
            FFBE::HandEquipment::ShieldTypeReverseMap_t >(
                flag , delimiter ,
                FFBE::HandEquipment::shieldTypeReverseMap );
    }

    HeadEquipment::HeadEquipmentType Character::getHeadEquipmentType() const
    {
        return _equipableFlags.Head;
    }

    void Character::setHeadEquipmentType( const HeadEquipment::HeadEquipmentType flag )
    {
        _equipableFlags.Head = flag;
    }

    void Character::setHeadEquipmentType( const std::string& flag , char delimiter )
    {
        _equipableFlags.Head = FFBE::ConvertStringToEquipmentFlag<
            FFBE::HeadEquipment::HeadEquipmentType ,
            FFBE::HeadEquipment::HeadEquipmentReverseMap_t >(
                flag , delimiter ,
                FFBE::HeadEquipment::headEquipmentTypeReverseMap );
    }

    BodyEquipment::BodyEquipmentType Character::getBodyEquipmentType() const
    {
        return _equipableFlags.Body;
    }

    void Character::setBodyEquipmentType( const BodyEquipment::BodyEquipmentType flag )
    {
        _equipableFlags.Body = flag;
    }

    void Character::setBodyEquipmentType( const std::string& flag , char delimiter )
    {
        _equipableFlags.Body = FFBE::ConvertStringToEquipmentFlag<
            FFBE::BodyEquipment::BodyEquipmentType ,
            FFBE::BodyEquipment::BodyEquipmentTypeReverseMap_t >(
                flag , delimiter ,
                FFBE::BodyEquipment::bodyEquipmentTypeReverseMap );
    }

    void Character::setEquipmentOptimizer( const std::shared_ptr<EquipmentOptimizer>& equipmentOptimizer )
    {
        _equipmentOptimizer = std::move( equipmentOptimizer );
    }

    #pragma endregion

    #pragma region Equipment Methods

    const std::shared_ptr<Equipment>& Character::getRHandEquipment() const
    {
        return _rightHand;
    }

    void Character::equipRHand( const std::shared_ptr<Equipment>& equipment )
    {
        _rightHand = equipment;
    }

    const std::shared_ptr<Equipment>& Character::getLHandEquipment() const
    {
        return _leftHand;
    }

    void Character::equipLHand( const std::shared_ptr<Equipment>& equipment )
    {
        _leftHand = equipment;
    }

    const std::shared_ptr<Equipment>& Character::getHeadEquipment() const
    {
        return _head;
    }

    void Character::equipHead( const std::shared_ptr<Equipment>& equipment )
    {
        _head = equipment;
    }

    const std::shared_ptr<Equipment>& Character::getBodyEquipment() const
    {
        return _body;
    }

    void Character::equipBody( const std::shared_ptr<Equipment>& equipment )
    {
        _body = equipment;
    }

    const std::shared_ptr<Equipment>& Character::getAccessory1Equipment() const
    {
        return _accessory1;
    }

    void Character::equipAccessory1( const std::shared_ptr<Equipment>& equipment )
    {
        _accessory1 = equipment;
    }

    const std::shared_ptr<Equipment>& Character::getAccessory2Equipment() const
    {
        return _accessory2;
    }

    void Character::equipAccessory2( const std::shared_ptr<Equipment>& equipment )
    {
        _accessory2 = equipment;
    }

    #pragma endregion

    #pragma region Optimize Equipment Methods

    bool Character::CanEquip( 
        const std::shared_ptr<Equipment>& equipment , 
        const Equipment::EquipmentType& type )
    {
        bool canEquip = false;
        uint32_t matchEquipmentType = 0;

        switch( type )
        {
            case Equipment::EquipmentType::HAND_EQUIPMENT:
                HandEquipment::WeaponType weaponFlag;
                weaponFlag = _equipableFlags.Weapon & std::dynamic_pointer_cast<HandEquipment>( equipment )->getWeaponType();
                HandEquipment::ShieldType shieldFlag;
                shieldFlag = _equipableFlags.Shield & std::dynamic_pointer_cast<HandEquipment>( equipment )->getShieldType();

                matchEquipmentType = uint32_t( weaponFlag ) | uint32_t( shieldFlag );
                break;
            case Equipment::EquipmentType::HEAD_EQUIPMENT:
                matchEquipmentType = uint32_t( _equipableFlags.Head & std::dynamic_pointer_cast<HeadEquipment>( equipment )->getHeadEquipmentType() );
                break;
            case Equipment::EquipmentType::BODY_EQUIPMENT:
                matchEquipmentType = uint32_t( _equipableFlags.Body & std::dynamic_pointer_cast<BodyEquipment>( equipment )->getBodyEquipmentType() );
                break;
            case Equipment::EquipmentType::ACCESSORY:
                matchEquipmentType = uint32_t( 1 );
                break;
        }

        return ( matchEquipmentType != 0 );
    }

    void Character::OptimizeRHandEquipment(
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        auto leftHand = std::dynamic_pointer_cast<HandEquipment>( _leftHand );
        if( leftHand->getHandleType() != HandEquipment::HandleType::TWOHANDED_EQUIPMENT &&
            _equipmentOptimizer.get() != nullptr &&
            _equipmentOptimizer->rightHand.get() != nullptr )
        {
            _equipmentOptimizer->rightHand->optimizeEquipment(
                _rightHand , _equipableFlags , equipmentList );
        }
    }

    void Character::OptimizeLHandEquipment(
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        auto rightHand = std::dynamic_pointer_cast<HandEquipment>( _rightHand );

        if( _bCanDualWield )
        {
            if( rightHand->getHandleType() != HandEquipment::HandleType::TWOHANDED_EQUIPMENT &&
                _equipmentOptimizer.get() != nullptr &&
                _equipmentOptimizer->leftHand.get() != nullptr )
            {
                _equipmentOptimizer->rightHand->optimizeEquipment(
                    _leftHand , _equipableFlags , equipmentList );
            }
        }
        else
        {
            if( rightHand->getHandleType() != HandEquipment::HandleType::TWOHANDED_EQUIPMENT &&
                _equipmentOptimizer.get() != nullptr &&
                _equipmentOptimizer->leftHand.get() != nullptr )
            {
                _equipmentOptimizer->leftHand->optimizeEquipment(
                    _leftHand , _equipableFlags , equipmentList );
            }
        }
    }

    void Character::OptimizeHeadEquipment( 
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        if( _equipmentOptimizer.get() != nullptr &&
            _equipmentOptimizer->head.get() != nullptr )
        {
            _equipmentOptimizer->head->optimizeEquipment(
                _head , _equipableFlags , equipmentList );
        }
    }

    void Character::OptimizeBodyEquipment( 
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        if( _equipmentOptimizer.get() != nullptr &&
            _equipmentOptimizer->body.get() != nullptr )
        {
            _equipmentOptimizer->body->optimizeEquipment(
                _body , _equipableFlags , equipmentList );
        }
    }

    void Character::OptimizeAccessory1Equipment( 
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        if( _equipmentOptimizer.get() != nullptr &&
            _equipmentOptimizer->accessory1.get() != nullptr )
        {
            _equipmentOptimizer->accessory1->optimizeEquipment(
                _accessory1 , _equipableFlags , equipmentList );
        }
    }

    void Character::OptimizeAccessory2Equipment( 
        const std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        if( _equipmentOptimizer.get() != nullptr &&
            _equipmentOptimizer->accessory2.get() != nullptr )
        {
            _equipmentOptimizer->accessory2->optimizeEquipment(
                _accessory2 , _equipableFlags , equipmentList );
        }
    }

    #pragma endregion

    #pragma region Public Methods

    std::string Character::ToString()
    {
        return _name;
    }

    std::string Character::getCharacterInfo() const
    {
        std::stringstream ss;
        int fieldSize = 10;

        ss << std::setiosflags( std::ios::left );
        ss << "\n--------------------------------------------------" << std::endl;
        ss << _name << "'s Information" << std::endl;
        ss << "--------------------------------------------------" << std::endl;

        ss << std::setw( fieldSize ) << "R.Hand:"
            << ( ( _rightHand == nullptr ) ? "None" : _rightHand->ToString() )
            << std::endl;

        ss << std::setw( fieldSize ) << "L.Hand:"
            << ( ( _leftHand == nullptr ) ? "None" : _leftHand->ToString() )
            << std::endl;

        auto head = _head;
        ss << std::setw( fieldSize ) << "Head: "
            << ( ( _head == nullptr ) ? "None" : head->ToString() )
            << std::endl;

        auto body = _body;
        ss << std::setw( fieldSize ) << "Body: "
            << ( ( _body == nullptr ) ? "None" : body->ToString() )
            << std::endl;

        auto accessory1 = _accessory1;
        ss << std::setw( fieldSize ) << "Acc. 1: "
            << ( ( _accessory1 == nullptr ) ? "None" : accessory1->ToString() )
            << std::endl;

        auto accessory2 = _accessory2;
        ss << std::setw( fieldSize ) << "Acc. 2: "
            << ( ( _accessory2 == nullptr ) ? "None" : accessory2->ToString() )
            << std::endl;

        ss << std::endl;
        
        ss << "Equippable Weaponry: ";
        ss << FFBE::ConvertEquipmentFlagToString<
            HandEquipment::WeaponType ,
            HandEquipment::WeaponTypeMap_t>(
                _equipableFlags.Weapon , ',' ,
                HandEquipment::weaponTypeMap ) << std::endl;

        ss << "Equippable Shiled: ";
        ss << FFBE::ConvertEquipmentFlagToString<
            HandEquipment::ShieldType ,
            HandEquipment::ShieldTypeMap_t>(
                _equipableFlags.Shield , ',' ,
                HandEquipment::shieldTypeMap ) << std::endl;
        
        ss << "Equippable Headwear: ";
        ss << FFBE::ConvertEquipmentFlagToString<
            HeadEquipment::HeadEquipmentType ,
            HeadEquipment::HeadEquipmentMap_t>(
                _equipableFlags.Head , ',' ,
                HeadEquipment::headEquipmentTypeMap ) << std::endl;

        ss << "Equippable Bodywear: ";
        ss << FFBE::ConvertEquipmentFlagToString<
            BodyEquipment::BodyEquipmentType ,
            BodyEquipment::BodyEquipmentTypeMap_t>(
                _equipableFlags.Body , ',' ,
                BodyEquipment::bodyEquipmentTypeMap ) << std::endl;

        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    #pragma endregion

    #pragma region Enumerable Overloading Operators

    Character::RoleType operator|( const Character::RoleType & lhs , const Character::RoleType & rhs )
    {
        return static_cast<Character::RoleType>( ( uint32_t( lhs ) | uint32_t( rhs ) ) );
    }

    Character::RoleType operator&( const Character::RoleType & lhs , const Character::RoleType & rhs )
    {
        return static_cast<Character::RoleType>( ( uint32_t( lhs ) & uint32_t( rhs ) ) );
    }

    #pragma endregion

}