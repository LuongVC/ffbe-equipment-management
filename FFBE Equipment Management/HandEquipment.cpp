#include "HandEquipment.h"

namespace FFBE
{

    #pragma region Enumerable Maps

    HandEquipment::WeaponTypeMap_t HandEquipment::weaponTypeMap =
    {
        { WeaponType::NONE           , "None" },
        { WeaponType::DAGGER         , "Dagger" },
        { WeaponType::SWORD          , "Sword" },
        { WeaponType::GREAT_SWORD    , "Great Sword" },
        { WeaponType::KATANA         , "Katana" },
        { WeaponType::STAFF          , "Staff" },
        { WeaponType::ROD            , "Rod" },
        { WeaponType::BOW            , "Bow" },
        { WeaponType::AXE            , "Axe" },
        { WeaponType::HAMMER         , "Hammer" },
        { WeaponType::SPEAR          , "Spear" },
        { WeaponType::HARP           , "Harp" },
        { WeaponType::WHIP           , "Whip" },
        { WeaponType::THROWING_WEAPON    , "Throwing Weapon" },
        { WeaponType::GUN            , "Gun" },
        { WeaponType::MACE           , "Mace" },
        { WeaponType::FIST           , "Fist" }
    };

    HandEquipment::WeaponTypeReverseMap_t HandEquipment::weaponTypeReverseMap =
    {
        { "None"            , WeaponType::NONE },
        { "Dagger"          , WeaponType::DAGGER },
        { "Sword"           , WeaponType::SWORD },
        { "Great Sword"     , WeaponType::GREAT_SWORD },
        { "Katana"          , WeaponType::KATANA },
        { "Staff"           , WeaponType::STAFF },
        { "Rod"             , WeaponType::ROD },
        { "Bow"             , WeaponType::BOW },
        { "Axe"             , WeaponType::AXE },
        { "Hammer"          , WeaponType::HAMMER },
        { "Spear"           , WeaponType::SPEAR },
        { "Harp"            , WeaponType::HARP },
        { "Whip"            , WeaponType::WHIP },
        { "Throwing Weapon" , WeaponType::THROWING_WEAPON },
        { "Gun"             , WeaponType::GUN },
        { "Mace"            , WeaponType::MACE },
        { "Fist"            , WeaponType::FIST }
    };

    HandEquipment::ShieldTypeMap_t HandEquipment::shieldTypeMap =
    {
        { ShieldType::NONE         , "None" },
        { ShieldType::LIGHT_SHIELD , "Light Shield" },
        { ShieldType::HEAVY_SHIELD , "Heavy Shield" }
    };

    HandEquipment::ShieldTypeReverseMap_t HandEquipment::shieldTypeReverseMap =
    {
        { "None"            , ShieldType::NONE },
        { "Light Shield"    , ShieldType::LIGHT_SHIELD },
        { "Heavy Shield"    , ShieldType::HEAVY_SHIELD }
    };

    HandEquipment::HandleType_t HandEquipment::handleTypeMap =
    {
        { HandleType::NONE , "" },
        { HandleType::ONEHANDED_EQUIPMENT , "1H" },
        { HandleType::TWOHANDED_EQUIPMENT , "2H" }
    };

    #pragma endregion

    #pragma region Constructors & Destructor

    HandEquipment::HandEquipment()
    {
        setEquipmentType( EquipmentType::HAND_EQUIPMENT );
        setWeaponInfo();
    }

    HandEquipment::HandEquipment( 
        std::string name ,
        WeaponType type , HandleType handle ,
        int hp , int mp ,
        int str , int mag ,
        int def , int spr )
    {
        setEquipmentType( EquipmentType::HAND_EQUIPMENT );
        setWeaponInfo( name , type , handle );
        setStats( hp , mp , str , mag , def , spr );
    }

    HandEquipment::HandEquipment( 
        std::string name , 
        ShieldType type , HandleType handle , 
        int hp , int mp , 
        int str , int mag , 
        int def , int spr )
    {
        setEquipmentType( EquipmentType::HAND_EQUIPMENT );
        setShieldInfo( name , type , handle );
        setStats( hp , mp , str , mag , def , spr );
    }

    HandEquipment::HandEquipment( const HandEquipment& newHandEquipment )
    {
        *this = newHandEquipment;
    }

    #pragma endregion

    #pragma region Constructors & Destructors Helper Functions

    void HandEquipment::setWeaponInfo(
        std::string name ,
        WeaponType weaponType ,
        HandleType handle )
    {
        setName( name );

        _weaponType = weaponType;
        _shieldType = ShieldType::NONE;
        _handleType = handle;
    }

    void HandEquipment::setShieldInfo(
        std::string name ,
        ShieldType shieldType ,
        HandleType handle )
    {
        setName( name );

        _weaponType = WeaponType::NONE;
        _shieldType = shieldType;
        _handleType = handle;
    }

    #pragma endregion

    #pragma region Getters & Setters

    HandEquipment::WeaponType HandEquipment::getWeaponType() const
    {
        return _weaponType;
    }

    void HandEquipment::setWeaponType( const WeaponType type )
    {
        _weaponType = type;
    }

    HandEquipment::ShieldType HandEquipment::getShieldType() const
    {
        return _shieldType;
    }

    void HandEquipment::setShieldType( const ShieldType type )
    {
        _shieldType = type;
    }

    HandEquipment::HandleType HandEquipment::getHandleType() const
    {
        return _handleType;
    }

    void HandEquipment::setHandleType( const HandleType type )
    {
        _handleType = type;
    }

    #pragma endregion

    #pragma region Overloading Operators

    HandEquipment& HandEquipment::operator=( const HandEquipment& rhs )
    {
        if( this != &rhs )
        {
            setName( rhs.getName() );
            setStats( rhs.getStats() );

            _weaponType = rhs.getWeaponType();
            _shieldType = rhs.getShieldType();
            _handleType = rhs.getHandleType();
        }

        return *this;
    }

    #pragma endregion

    #pragma region Public Methods

    std::string HandEquipment::GetEquipmentDetail()
    {
        std::stringstream ss;
        ss << std::setiosflags( std::ios::left );
        ss << std::setw( 32 ) << Equipment::ToString();
        ss << std::setw( 3 ) << handleTypeMap[ _handleType ]
            << std::setw( 17 ) << weaponTypeMap[ _weaponType ];
        ss << getStats().ToString();
        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    std::string HandEquipment::ToString()
    {
        return getName();
    }

    #pragma endregion

    #pragma region Enumerable Overloading Operators

    HandEquipment::WeaponType operator|(
        const HandEquipment::WeaponType& lhs ,
        const HandEquipment::WeaponType& rhs )
    {
        return static_cast<HandEquipment::WeaponType>( 
            ( uint32_t( lhs ) | uint32_t( rhs ) ) );
    }

    HandEquipment::WeaponType operator&(
        const HandEquipment::WeaponType& lhs ,
        const HandEquipment::WeaponType& rhs )
    {
        return static_cast<HandEquipment::WeaponType>(
            ( uint32_t( lhs ) & uint32_t( rhs ) ) );
    }

    HandEquipment::ShieldType operator|( 
        const HandEquipment::ShieldType& lhs , 
        const HandEquipment::ShieldType& rhs )
    {
        return static_cast<HandEquipment::ShieldType>(
            ( uint32_t( lhs ) | uint32_t( rhs ) ) );
    }

    HandEquipment::ShieldType operator&( 
        const HandEquipment::ShieldType& lhs , 
        const HandEquipment::ShieldType& rhs )
    {
        return static_cast<HandEquipment::ShieldType>(
            ( uint32_t( lhs ) & uint32_t( rhs ) ) );
    }

    #pragma endregion

}