#include "Stats.h"

namespace FFBE
{

    #pragma region Setters & Getters

    void Stats::SetStats( const int hp , const int mp , const int atk , const int mag , const int def , const int spr )
    {
        this->hp = hp;
        this->mp = mp;
        this->atk = atk;
        this->mag = mag;
        this->def = def;
        this->spr = spr;
    }

    void Stats::SetStats( const Stats& newStats )
    {
        *this = newStats;
    }

    Stats Stats::GetStats()
    {
        return *this;
    }

    #pragma endregion

    #pragma region Overloading Operators

    Stats& Stats::operator=( const Stats& newStats )
    {
        if( this != &newStats )
        {
            SetStats(
                newStats.hp , newStats.mp ,
                newStats.atk , newStats.mag ,
                newStats.def , newStats.spr
            );
        }

        return *this;
    }

    #pragma endregion

    #pragma region Public Methods

    std::string Stats::ToString()
    {
        std::stringstream ss;
        ss << std::setiosflags( std::ios::right );
        ss << std::setw( 3 ) << hp << " HP, "
            << std::setw( 3 ) << mp << " MP, "
            << std::setw( 3 ) << atk << " ATK, "
            << std::setw( 3 ) << mag << " MAG, "
            << std::setw( 3 ) << def << " DEF, "
            << std::setw( 3 ) << spr << " SPR";
        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    #pragma endregion

}