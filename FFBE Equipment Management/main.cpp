#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
#include <list>
#include <fstream>

#include <Windows.h>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "Shop.h"
#include "Character.h"
#include "CharacterList.h"
#include "Utility.h"

std::string ExePath();

int main( int argc , char* argv[] )
{
    FFBE::Shop shop;
    auto weaponList = std::move( shop.getWeaponList() );
    auto shieldList = std::move( shop.getShieldList() );
    auto headwearList = std::move( shop.getHeadwearList() );
    auto bodywearList = std::move( shop.getBodywearList() );
    auto accessoryList = std::move( shop.getAccessoryList() );

    std::cout << "Shop Equipment List: " << std::endl;

    auto lambdaPrintList = []( std::shared_ptr<FFBE::Equipment> eq ) { std::cout << eq->GetEquipmentDetail() << std::endl; };
    std::for_each( weaponList.begin() , weaponList.end() , lambdaPrintList );
    std::for_each( headwearList.begin() , headwearList.end() , lambdaPrintList );
    std::for_each( bodywearList.begin() , bodywearList.end() , lambdaPrintList );
    std::for_each( accessoryList.begin() , accessoryList.end() , lambdaPrintList );

    std::cout << std::endl;

    std::string characterListFilepath = "../FFBE Equipment Management/CharacterList.json";

    std::cout << "Current Directory: " << ExePath() << std::endl;

    FFBE::CharacterList characterList;
    characterList.loadCharacterFromFile( characterListFilepath );
    characterList.OptimizeRHandEquipment( weaponList );
    characterList.OptimizeLHandEquipment( shieldList );
    characterList.OptimizeLHandEquipment( weaponList );
    characterList.OptimizeHeadEquipment( headwearList );
    characterList.OptimizeBodyEquipment( bodywearList );
    characterList.OptimizeAccessory1Equipment( accessoryList );
    characterList.OptimizeAccessory2Equipment( accessoryList );

    characterList.printAllCharacterDetail();
    characterList.printAllEquipmentUsed();

    return 0;
}

std::string ExePath()
{
    // Get executable path
    WCHAR buffer[ MAX_PATH ] = { 0 };
    GetModuleFileName( NULL , buffer , MAX_PATH );

    // convert the (unicode) wide string to string
    std::wstring wString = std::wstring( buffer );
    std::string sBuffer = std::string( wString.begin() , wString.end() );

    std::string::size_type pos = std::string( sBuffer ).find_last_of( "\\/" );
    return std::string( sBuffer ).substr( 0 , pos );
}