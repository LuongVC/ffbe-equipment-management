#include "OptimizeAccessoryEquipment.h"

namespace FFBE
{

    #pragma region Constructors & Destructor

    OptimizeAccessoryEquipment::OptimizeAccessoryEquipment(
        Stats::StatType first ,
        Stats::StatType second ,
        Stats::StatType third ,
        Stats::StatType fourth ,
        Stats::StatType fifth ,
        Stats::StatType sixth )
    {
        _statsPriority.first = first;
        _statsPriority.second = second;
        _statsPriority.third = third;
        _statsPriority.fourth = fourth;
        _statsPriority.fifth = fifth;
        _statsPriority.sixth = sixth;
    }

    #pragma endregion

    #pragma region Override Methods

    bool OptimizeAccessoryEquipment::canEquip( 
        const std::shared_ptr<Equipment> equipment , 
        const EquipableFlags & equipableEquipmentFlag ) const
    {
        return true;
    }

    #pragma endregion
}