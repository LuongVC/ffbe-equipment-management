#include "EquipmentOptimizer.h"

namespace FFBE
{

    #pragma region Constructors & Destructor (MeleeEquipmentOptimizer)

    MeleeEquipmentOptimizer::MeleeEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::ATK );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::DEF );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (BerserkEquipmentOptimizer)

    BerserkEquipmentOptimizer::BerserkEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::ATK );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::ATK );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::ATK );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (TankEquipmentOptimizer)

    TankEquipmentOptimizer::TankEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::ATK );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (RangeEquipmentOptimizer)

    RangeEquipmentOptimizer::RangeEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::ATK );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::ATK );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::DEF );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (MageEquipmentOptimizer)

    MageEquipmentOptimizer::MageEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::MAG );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::MAG , Stats::StatType::SPR );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::MAG , Stats::StatType::SPR );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::DEF );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (SupportEquipmentOptimizer)

    SupportEquipmentOptimizer::SupportEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::ATK );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::DEF );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

    #pragma region Constructors & Destructor (HealerEquipmentOptimizer)

    HealerEquipmentOptimizer::HealerEquipmentOptimizer()
    {
        rightHand   = std::make_shared<OptimizeWeaponEquipment>( Stats::StatType::SPR );
        leftHand    = std::make_shared<OptimizeShieldEquipment>( Stats::StatType::DEF );
        head        = std::make_shared<OptimizeHeadEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        body        = std::make_shared<OptimizeBodyEquipment>( Stats::StatType::DEF , Stats::StatType::SPR );
        accessory1  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::DEF );
        accessory2  = std::make_shared<OptimizeAccessoryEquipment>( Stats::StatType::SPR );
    }

    #pragma endregion

}