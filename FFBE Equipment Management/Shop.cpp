#include "Shop.h"

namespace FFBE
{

    #pragma region Constructors & Destructors

    Shop::Shop()
    {
        PopulateWeaponList();
        PopulateShieldList();
        PopulateHeadwearList();
        PopulateBodywearList();
        PopulateAccessoryList();
    }

    Shop::~Shop()
    {
    }

    #pragma endregion

    #pragma region Constructors & Destructors Helper Methods

    void Shop::PopulateWeaponList()
    {
        _weaponList.clear();

        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Dagger" ,
            HandEquipment::WeaponType::DAGGER ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 20 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Sword" ,
            HandEquipment::WeaponType::SWORD ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 26 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Saber" ,
            HandEquipment::WeaponType::GREAT_SWORD ,
            HandEquipment::HandleType::TWOHANDED_EQUIPMENT ,
            0 , 0 , 45 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Kazekiri" ,
            HandEquipment::WeaponType::KATANA ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 35 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Wizard's Staff" ,
            HandEquipment::WeaponType::STAFF ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 13 , 6 , 0 , 32 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Iron Rod" ,
            HandEquipment::WeaponType::ROD ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 10 , 18 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Bow" ,
            HandEquipment::WeaponType::BOW ,
            HandEquipment::HandleType::TWOHANDED_EQUIPMENT ,
            0 , 0 , 38 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Giant\'s Axe" ,
            HandEquipment::WeaponType::AXE ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 50 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Hammer" ,
            HandEquipment::WeaponType::HAMMER ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 47 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Spear" ,
            HandEquipment::WeaponType::SPEAR ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 40 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Harp" ,
            HandEquipment::WeaponType::HARP ,
            HandEquipment::HandleType::TWOHANDED_EQUIPMENT ,
            0 , 0 , 20 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Chain Whip" ,
            HandEquipment::WeaponType::WHIP ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 21 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Moon Ring Blade" ,
            HandEquipment::WeaponType::THROWING_WEAPON ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 26 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Sirius" ,
            HandEquipment::WeaponType::GUN ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 46 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Mace" ,
            HandEquipment::WeaponType::MACE ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 30 , 0 , 0 , 0 ) );
        _weaponList.push_back( std::make_shared<HandEquipment>(
            "Mythril Claws" ,
            HandEquipment::WeaponType::FIST ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 42 , 0 , 0 , 0 ) );
    }

    void Shop::PopulateShieldList()
    {
        _shieldList.clear();

        _shieldList.push_back( std::make_shared<HandEquipment>(
            "Mythril Buckler" ,
            HandEquipment::ShieldType::LIGHT_SHIELD ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 0 , 0 , 30 , 0 ) );
        _shieldList.push_back( std::make_shared<HandEquipment>(
            "Mythril Shield" ,
            HandEquipment::ShieldType::HEAVY_SHIELD ,
            HandEquipment::HandleType::ONEHANDED_EQUIPMENT ,
            0 , 0 , 0 , 0 , 35 , 0 ) );
    }

    void Shop::PopulateHeadwearList()
    {
        _headwearList.clear();

        _headwearList.push_back( std::make_shared<HeadEquipment>(
            "Celebrant\'s Miter",
            HeadEquipment::HeadEquipmentType::HAT,
            0, 0, 0, 0, 10, 10 ) );
        _headwearList.push_back( std::make_shared<HeadEquipment>(
            "Green Beret" ,
            HeadEquipment::HeadEquipmentType::HAT ,
            50 , 0 , 10 , 0 , 10 , 0 ) );
        _headwearList.push_back( std::make_shared<HeadEquipment>(
            "Mythril Helm" ,
            HeadEquipment::HeadEquipmentType::HELMET ,
            0 , 0 , 0 , 0 , 20 , 0 ) );
    }

    void Shop::PopulateBodywearList()
    {
        _bodywearList.clear();

        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Scholar Robe" ,
            BodyEquipment::BodyEquipmentType::CLOTH ,
            0 , 20 , 0 , 0 , 13 , 10
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Ninja Gear" ,
            BodyEquipment::BodyEquipmentType::CLOTH ,
            0 , 0 , 4 , 0 , 20 , 0
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Silver Cuirass" ,
            BodyEquipment::BodyEquipmentType::LIGHT_ARMOUR ,
            0 , 0 , 0 , 0 , 34 , 0
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Mythril Armor" ,
            BodyEquipment::BodyEquipmentType::HEAVY_ARMOUR ,
            0 , 0 , 0 , 0 , 38 , 0
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Wizard\'s Robe" ,
            BodyEquipment::BodyEquipmentType::ROBE ,
            0 , 0 , 0 , 12 , 18 , 0
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Sage\'s Surplice" ,
            BodyEquipment::BodyEquipmentType::ROBE ,
            0 , 0 , 0 , 0 , 18 , 12
        ) );
        _bodywearList.push_back( std::make_shared<BodyEquipment>(
            "Gaia Gear" ,
            BodyEquipment::BodyEquipmentType::ROBE ,
            0 , 0 , 0 , 5 , 20 , 5
        ) );
    }

    void Shop::PopulateAccessoryList()
    {
        _accessoryList.clear();

        _accessoryList.push_back( std::make_shared<Equipment>(
            "Power Wrist" ,
            Equipment::EquipmentType::ACCESSORY ,
            0 , 0 , 5 , 0 , 0 , 0 ) );
        _accessoryList.push_back( std::make_shared<Equipment>(
            "Iron Gloves" ,
            Equipment::EquipmentType::ACCESSORY ,
            0 , 0 , 0 , 0 , 5 , 0 ) );
        _accessoryList.push_back( std::make_shared<Equipment>(
            "Silver Armlet" ,
            Equipment::EquipmentType::ACCESSORY ,
            0 , 15 , 0 , 0 , 3 , 0 ) );
        _accessoryList.push_back( std::make_shared<Equipment>(
            "Protect Ring" ,
            Equipment::EquipmentType::ACCESSORY ,
            0 , 0 , 0 , 0 , 8 , 0 ) );
        _accessoryList.push_back( std::make_shared<Equipment>(
            "Barrier Ring" ,
            Equipment::EquipmentType::ACCESSORY ,
            0 , 0 , 0 , 0 , 0 , 8 ) );
        _accessoryList.push_back( std::make_shared<Equipment>(
            "Regen Ring" ,
            Equipment::EquipmentType::ACCESSORY ,
            80 , 0 , 0 , 0 , 0 , 0 ) );
    }

    #pragma endregion

    #pragma region Setters & Getters

    std::list<std::shared_ptr<Equipment>>const& Shop::getWeaponList() const
    {
        return _weaponList;
    }

    void Shop::setWeaponList( std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        _weaponList = std::move( equipmentList );
    }

    std::list<std::shared_ptr<Equipment>>const& Shop::getShieldList() const
    {
        return _shieldList;
    }

    void Shop::setShieldList( std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        _shieldList = std::move( equipmentList );
    }

    std::list<std::shared_ptr<Equipment>>const& Shop::getHeadwearList() const
    {
        return _headwearList;
    }

    void Shop::setHeadwearList( std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        _headwearList = std::move( equipmentList );
    }

    std::list<std::shared_ptr<Equipment>>const& Shop::getBodywearList() const
    {
        return _bodywearList;
    }

    void Shop::setBodywearList( std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        _bodywearList = std::move( equipmentList );
    }

   
    std::list<std::shared_ptr<Equipment>>const& Shop::getAccessoryList() const
    {
        return _accessoryList;
    }

    void Shop::setAccessoryList( std::list<std::shared_ptr<Equipment>>& equipmentList )
    {
        _accessoryList = std::move( equipmentList );
    }

    #pragma endregion

}