#ifndef STATSPRIORITY_H
#define STATSPRIORITY_H

#include "Stats.h"

namespace FFBE
{

    struct StatsPriority
    {
        static const double firstMultiplier;
        static const double secondMultiplier;
        static const double thirdMultiplier;
        static const double fourthMultiplier;
        static const double fifthMultiplier;
        static const double sixthMultiplier;

        Stats::StatType first   = Stats::StatType::UKNOWN_STAT_TYPE;
        Stats::StatType second  = Stats::StatType::UKNOWN_STAT_TYPE;
        Stats::StatType third   = Stats::StatType::UKNOWN_STAT_TYPE;
        Stats::StatType fourth  = Stats::StatType::UKNOWN_STAT_TYPE;
        Stats::StatType fifth   = Stats::StatType::UKNOWN_STAT_TYPE;
        Stats::StatType sixth   = Stats::StatType::UKNOWN_STAT_TYPE;
    };

}

#endif // !STATSPRIORITY_H