#ifndef EQUIPMENTOPTIMIZER_H
#define EQUIPMENTOPTIMIZER_H

#include <memory>

#include "OptimizeEquipment.h"
#include "OptimizeWeaponEquipment.h"
#include "OptimizeShieldEquipment.h"
#include "OptimizeHeadEquipment.h"
#include "OptimizeBodyEquipment.h"
#include "OptimizeAccessoryEquipment.h"

namespace FFBE
{

    struct EquipmentOptimizer
    {

    #pragma region Data Members

    public:
        std::shared_ptr<OptimizeEquipment> leftHand;
        std::shared_ptr<OptimizeEquipment> rightHand;
        std::shared_ptr<OptimizeEquipment> head;
        std::shared_ptr<OptimizeEquipment> body;
        std::shared_ptr<OptimizeEquipment> accessory1;
        std::shared_ptr<OptimizeEquipment> accessory2;

    #pragma endregion

    };

    class MeleeEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        MeleeEquipmentOptimizer();
        virtual ~MeleeEquipmentOptimizer() { };

    #pragma endregion

    };

    class BerserkEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        BerserkEquipmentOptimizer();
        virtual ~BerserkEquipmentOptimizer() { };

    #pragma endregion

    };

    class TankEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        TankEquipmentOptimizer();
        virtual ~TankEquipmentOptimizer() { };

    #pragma endregion

    };

    class RangeEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        RangeEquipmentOptimizer();
        virtual ~RangeEquipmentOptimizer() { };

    #pragma endregion

    };

    class MageEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        MageEquipmentOptimizer();
        virtual ~MageEquipmentOptimizer() { };

    #pragma endregion

    };

    class SupportEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        SupportEquipmentOptimizer();
        virtual ~SupportEquipmentOptimizer() { };

    #pragma endregion

    };

    class HealerEquipmentOptimizer : public EquipmentOptimizer
    {

    #pragma region Constructors & Destructor

    public:
        HealerEquipmentOptimizer();
        virtual ~HealerEquipmentOptimizer() { };

    #pragma endregion

    };
}

#endif // !EQUIPMENTOPTIMIZER_H