#ifndef BODYEQUIPMENT_H
#define BODYEQUIPMENT_H

#include <string>
#include <sstream>
#include <map>

#include "Equipment.h"
#include "InsensitiveCaseComparer.h"

namespace FFBE
{

    class BodyEquipment : public Equipment
    {

    #pragma region Enumerables & Enumerable Maps

    public:
        enum class BodyEquipmentType
        {
            NONE            = 0 ,
            CLOTH           = ( 1 << 0 ) ,
            LIGHT_ARMOUR    = ( 1 << 1 ) ,
            HEAVY_ARMOUR    = ( 1 << 2 ) ,
            ROBE            = ( 1 << 3 )
        };

        typedef std::map<BodyEquipmentType , std::string> BodyEquipmentTypeMap_t;
        static BodyEquipmentTypeMap_t bodyEquipmentTypeMap;

        typedef std::map<std::string , BodyEquipmentType , InsensitiveCaseCompare> BodyEquipmentTypeReverseMap_t;
        static BodyEquipmentTypeReverseMap_t bodyEquipmentTypeReverseMap;

    #pragma endregion

    #pragma region Data Members
        
    private:
        BodyEquipmentType _bodyEquipmentType;

    #pragma endregion

    #pragma region Constructors & Destructor

    public:
        BodyEquipment();
        BodyEquipment( 
            std::string name , BodyEquipmentType type ,
            int hp , int mp , int atk , int mag , int def , int spr );
        BodyEquipment( const BodyEquipment& newBodyEquipment );

        virtual ~BodyEquipment() = default;

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    private:
        void setBodyEquipmentInfo( 
            std::string name = "None" , 
            BodyEquipmentType type = BodyEquipmentType::NONE );

    #pragma endregion

    #pragma region Getters & Setters

    public:
        BodyEquipmentType getBodyEquipmentType() const;
        void setBodyEquipmentType( const BodyEquipmentType& type );

    #pragma endregion

    #pragma region Overloading Operators

        BodyEquipment& operator=( const BodyEquipment& rhs );

    #pragma endregion

    #pragma region Public Methods

    public:
        std::string GetEquipmentDetail() override;
        std::string ToString() override;

    #pragma endregion

    };

    #pragma region Enumerable Overloading Operators

    BodyEquipment::BodyEquipmentType operator|(
        const BodyEquipment::BodyEquipmentType& lhs ,
        const BodyEquipment::BodyEquipmentType& rhs );

    BodyEquipment::BodyEquipmentType operator&(
        const BodyEquipment::BodyEquipmentType& lhs ,
        const BodyEquipment::BodyEquipmentType& rhs );

    #pragma endregion

}

#endif // !BODYEQUIPMENT_H