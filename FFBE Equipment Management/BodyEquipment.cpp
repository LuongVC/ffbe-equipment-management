#include "BodyEquipment.h"


namespace FFBE
{

    #pragma region Enumerable Maps

    BodyEquipment::BodyEquipmentTypeMap_t BodyEquipment::bodyEquipmentTypeMap =
    {
        { BodyEquipmentType::NONE           , "None" } ,
        { BodyEquipmentType::CLOTH          , "Cloth" } ,
        { BodyEquipmentType::LIGHT_ARMOUR   , "Light Armour" } ,
        { BodyEquipmentType::HEAVY_ARMOUR   , "Heavy Armour" } ,
        { BodyEquipmentType::ROBE           , "Robe" }
    };

    BodyEquipment::BodyEquipmentTypeReverseMap_t BodyEquipment::bodyEquipmentTypeReverseMap =
    {
        { "None"            , BodyEquipmentType::NONE } ,
        { "Cloth"           , BodyEquipmentType::CLOTH } ,
        { "Light Armour"    , BodyEquipmentType::LIGHT_ARMOUR } ,
        { "Heavy Armour"    , BodyEquipmentType::HEAVY_ARMOUR } ,
        { "Robe"            , BodyEquipmentType::ROBE }
    };

    #pragma endregion

    #pragma region Constructors & Destructor

    BodyEquipment::BodyEquipment()
    {
        setEquipmentType( EquipmentType::BODY_EQUIPMENT );
        setBodyEquipmentInfo();
    }

    BodyEquipment::BodyEquipment( 
        std::string name , BodyEquipmentType type ,
        int hp , int mp , int atk , int mag , int def , int spr )
    {
        setEquipmentType( EquipmentType::BODY_EQUIPMENT );
        setBodyEquipmentInfo( name , type );
        setStats( hp , mp , atk , mag , def , spr );
    }

    BodyEquipment::BodyEquipment( const BodyEquipment & newBodyEquipment )
    {
        *this = newBodyEquipment;
    }

    #pragma endregion

    #pragma region Constructors & Destructor Helper Functions

    void BodyEquipment::setBodyEquipmentInfo( std::string name , BodyEquipmentType type )
    {
        setName( name );
        
        _bodyEquipmentType = type;
    }

    #pragma endregion

    #pragma region Getters & Setters

    BodyEquipment::BodyEquipmentType BodyEquipment::getBodyEquipmentType() const
    {
        return _bodyEquipmentType;
    }

    void BodyEquipment::setBodyEquipmentType( const BodyEquipmentType & type )
    {
        _bodyEquipmentType = type;
    }

    #pragma endregion

    #pragma region Overloading Operators

    BodyEquipment & BodyEquipment::operator=( const BodyEquipment & rhs )
    {
        if( this != &rhs )
        {
            setName( rhs.getName() );
            setStats( rhs.getStats() );

            _bodyEquipmentType = rhs.getBodyEquipmentType();
        }

        return *this;
    }

    #pragma endregion

    #pragma region Public Methods

    std::string BodyEquipment::GetEquipmentDetail()
    {
        std::stringstream ss;
        ss << std::setiosflags( std::ios::left );
        ss << std::setw( 32 ) << Equipment::ToString();
        ss << std::setw( 20 ) << bodyEquipmentTypeMap[ _bodyEquipmentType ];
        ss << getStats().ToString();
        ss << std::resetiosflags( std::ios_base::basefield );

        return ss.str();
    }

    std::string BodyEquipment::ToString()
    {
        return getName();
    }

    #pragma endregion

    #pragma region Enumerable Overloading Operators

    BodyEquipment::BodyEquipmentType operator|( 
        const BodyEquipment::BodyEquipmentType& lhs , 
        const BodyEquipment::BodyEquipmentType& rhs )
    {
        return static_cast<BodyEquipment::BodyEquipmentType>(
            ( uint32_t( lhs ) | uint32_t( rhs ) ) );
    }

    BodyEquipment::BodyEquipmentType operator&( 
        const BodyEquipment::BodyEquipmentType& lhs , 
        const BodyEquipment::BodyEquipmentType& rhs )
    {
        return static_cast<BodyEquipment::BodyEquipmentType>(
            ( uint32_t( lhs ) & uint32_t( rhs ) ) );
    }

    #pragma endregion

}